const fetch = require('node-fetch');
const base64 = require('base-64');
const Iron = require('@hapi/iron');

const COOKIE_KEY = 'sealedToken';

// polyfill
global.Headers = global.Headers || require('fetch-headers'); // eslint-disable-line global-require

/**
 * @return {string}
 */
function getIronPassword() {
    // TODO miky safe password - not part of repo
    return 'PT@Kz5a&9c]A;`R<56r&"s,=*ag#-4dv2DmgO~.{`IGKZ22@S,m;>{w@69-n)4t';
}

/**
 * @param {Express.Response} res
 * @param {string} sealedToken
 */
function saveTokenIntoCookies({ res, sealedToken }) {
    res.cookie(
        COOKIE_KEY,
        sealedToken, {
            maxAge: 60 * 60 * 24 * 30, // 30 days
            httpOnly: true,
        },
    );
}

/**
 * @param {Express.Request} req
 * @return {boolean}
 */
function hasTokenInCookies({ req }) {
    const sealedToken = req.cookies[COOKIE_KEY];

    return Boolean(sealedToken);
}

/**
 * @param {Express.Request} req
 * @param {Express.Response} res
 */
function logout({ req, res }) {
    res.clearCookie(COOKIE_KEY);
    delete req.session.tokenData;
}

/**
 * @param {string} sealedToken
 * @return {Promise<boolean | TokenData>}
 */
async function unsealToken({ sealedToken }) {
    try {
        return await Iron.unseal(
            sealedToken,
            getIronPassword(),
            Iron.defaults,
        );
    } catch (err) {
        // TODO miky better logging
        console.log('CATCH unsealed error', err.message); // eslint-disable-line no-console
        return false;
    }
}

/**
 * @param {Express.Request} req
 * @return {Promise<boolean|TokenData>}
 */
async function readTokenFromCookies({ req }) {
    const sealedToken = req.cookies[COOKIE_KEY];

    if (!sealedToken)
        return false;

    return unsealToken({ sealedToken });
}

/**
 * @typedef {Object} TokenData
 * @property {string} token
 * @property {string} tokenDescription
 * @property {string} isoDate
 */

/**
 * @param {TokenData} tokenData
 * @return {Promise<boolean | string>}
 */
async function sealToken(tokenData) {
    try {
        return await Iron.seal(
            tokenData,
            getIronPassword(),
            Iron.defaults,
        );
    } catch (err) {
        console.log(err.message); // eslint-disable-line no-console
        return false;
    }
}


/**
 * Protected route guard
 *   * session record present -> continue
 *   * do not poll server just to validate actual token
 *   * fetch error can invalidate token later
 *
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @param {NextFunction} next
 */
async function checkAuth(req, res, next) {
    // session record present -> continue
    // do not poll server just to validate actual token
    const sessionTokenData = req.session.tokenData;

    if (sessionTokenData) {
        next();
        return;
    }

    // check cookies for "sealed" token
    readTokenFromCookies({ req }).then(
        (cookiesTokenData) => {
            if (cookiesTokenData) {
                // next time from session
                req.session.tokenData = cookiesTokenData;
                next();
                return;
            }

            // no tokens - need to login with credentials
            res.redirect('/login');
        },
    );
}

/**
 * Always regenerate token
 * @param email
 * @param password
 * @return {Promise< JSON | boolean >}
 */
async function getToken({ email, password }) {
    try {
        const response = await fetch(
            'https://api.apiary.io/authorization',
            {
                method: 'POST',
                headers: new Headers({
                    Authorization: `Basic ${base64.encode(`${email}:${password}`)}`,
                    'Content-Type': 'application/json',
                }),
                body: JSON.stringify({
                    tokenDescription: encodeURIComponent('Web UI'),
                    tokenRegenerate: 'true',
                }),
            },
        );
        return response.json();
    } catch (err) {
        // TODO miky better logging
        console.log('CATCH fetch error', err.message); // eslint-disable-line no-console
        return false;
    }
}

module.exports = {
    checkAuth,
    getToken,
    sealToken,
    unsealToken,
    saveTokenIntoCookies,
    hasTokenInCookies,
    logout,
};
