module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es6: true,
    },
    extends: [
        'airbnb-base',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        ecmaVersion: 2018,
    },
    rules: {
        indent: ['error', 4],
        'linebreak-style': ['error', 'windows'],
        'nonblock-statement-body-position': ["error", "below"],
        curly: ["error", "multi"],
    },
    overrides: [
        // tests
        {
            files: [
                '**/*.spec.js',
            ],
            rules: {
                'no-console': ['off']
            },
            globals: {
                // tests
                describe: true,
                it: true,
            }
        }
    ],
};
