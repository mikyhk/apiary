const fetch = require('node-fetch');
const base64 = require('base-64');

// polyfill
global.Headers = global.Headers || require('fetch-headers'); // eslint-disable-line global-require

const username = 'jan.kovsky.michal@gmail.com';
const password = 'api4*ary';

async function getToken() {
    const response = await fetch(
        'https://api.apiary.io/authorization',
        {
            method: 'POST',
            headers: new Headers({
                Authorization: `Basic ${base64.encode(`${username}:${password}`)}`,
                'Content-Type': 'application/json',
            }),
            body: JSON.stringify({
                tokenDescription: 'test1',
                // tokenDescription: encodeURIComponent('Web UI'),
                tokenRegenerate: 'true',
            }),
        },
    );
    const data = await response.json();
    console.log(JSON.stringify(data, null, '\t')); // eslint-disable-line no-console

    return data.token;
}

async function getAPIs(token) {
    const response = await fetch(

        'https://api.apiary.io/me/apis',
        {
            method: 'GET',
            headers: new Headers({
                Authorization: `bearer ${token}`,
            }),
        },
    );
    const data = await response.json();
    console.log(JSON.stringify(data, null, '\t')); // eslint-disable-line no-console
    return data;
}

(async () => {
    const token = await getToken();
    const data = await getAPIs(token); // eslint-disable-line no-unused-vars
})();
