// delegate events
document.addEventListener('click', function (event) {

    var target = event.target;
    var body = document.body;

    // flash message removed on click
    if (
        target.parentNode === body &&
        target.matches('.flashMessage')
    ) {
        body.removeChild(target);
    }

}, false);
