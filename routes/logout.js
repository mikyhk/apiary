const express = require('express');
const auth = require('../auth/auth');

const router = express.Router();

const { checkAuth } = require('../auth/auth');

// logout confirmation
router.get(/* /logout */'/',
    checkAuth,
    (req, res) => res.render(
        'logout',
        { hasTokenInCookies: auth.hasTokenInCookies({ req }) },
    ));

// logout process
router.post(/* /logout */'/',
    (req, res) => {
        auth.logout({ req, res });
        res.redirect('/login');
    });

module.exports = router;
