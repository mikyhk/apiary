const express = require('express');
const { getData } = require('../services/indexData');

const router = express.Router();

const { checkAuth } = require('../auth/auth');

// home page (for authenticated user) with all the results
router.get('/',
    checkAuth,
    getData,
    (req, res) => res.render(
        'index',
        { data: req.data },
    ));

module.exports = router;
