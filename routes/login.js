const express = require('express');

const router = express.Router();
const auth = require('../auth/auth');

// login form
router.get(/* /login */'/',
    (req, res) => res.render('login'));


/**
 * @param {string} token
 * @param {string} tokenDescription
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @return {Promise<void>}
 */
async function saveAndRedirect({
    token, tokenDescription, req, res,
}) {
    const tokenData = {
        token,
        tokenDescription,
        isoDate: new Date().toISOString(),
    };

    // always save to session
    if (!req.session.tokenData)
        req.session.tokenData = tokenData;

    // optionally save to cookies "sealed"
    const { rememberMe } = req.body;
    if (rememberMe) {
        const sealedToken = await auth.sealToken(tokenData);
        auth.saveTokenIntoCookies({ res, sealedToken });
    }

    // success redirect to main page
    res.redirect('/');
}

/**
 * @param {string} message
 * @param {Express.Request} req
 * @param {Express.Response} res
 */
function wrongCredentials({ message, req, res }) {
    req.flash('error', message);
    // render again with wrong credentials
    res.render('login',
        {
            data: req.body,
            messages: { error: message },
        });
}

/**
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @return {Promise<void>}
 */
async function tryToLogin({ req, res }) {
    const apiResponse = await auth.getToken(req.body);
    const { token, tokenDescription, error } = apiResponse;

    /*
    RESPONSE EXAMPLES
    success request:
    {
        token: '883d46d57a5ae8045583167df1b7be5b',
        tokenDescription: 'Web%20UI',
        tokenUrl: 'https://api.apiary.io/authorization/Web%2520UI'
    }
    error request:
    {
        error: 'Unauthorized'
    }
    */

    // token obtained -> finish auth
    if (token) {
        saveAndRedirect({
            token, tokenDescription, req, res,
        });
        return;
    }

    // generic error message
    let message = 'Error occurred during communication with API.';

    // error message from API
    if (error && error === 'Unauthorized')
        message = 'Combination of email and password is not valid.';

    wrongCredentials({ message, req, res });
}

// login process
router.post(/* /login */'/',
    (req, res, next) => {
        const { email, password } = req.body;

        if (email && password) {
            tryToLogin({ req, res, next });
            return;
        }

        // just in case client "required" validation didn't worked
        // "never trust client"
        let message;
        if (!email && password)
            message = 'Please fill email and password.';
        else if (!email)
            message = 'Please fill email.';
        else
            message = 'Please fill password.';

        wrongCredentials({ message, req, res });
    });

module.exports = router;
