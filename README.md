### UI for Apiary’s public API

Simple NodeJS app
 * login (obtain token) / logout
 * list user's API Projects

Resources:
 * repository https://gitlab.com/mikyhk/apiary.git
 * demo https://apiary-3694.rostiapp.cz/
