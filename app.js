const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');
const flash = require('connect-flash');
const session = require('express-session');

const app = express();
app.use(helmet());
app.use(flash());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    cookie: { maxAge: 60000 },
    secret: '(Wk!vBG5*c}9Dhfli?KpZ/u?4w9Z+y9cwZth8mVHc[!_sz+V:79}vkx*k=.FLS[',
    resave: false,
    saveUninitialized: false,
}));

// globally accessible properties (for pug templates)
app.use((req, res, next) => {
    app.locals.flashMessages = req.flash() || {};
    next();
});

app.use('/', require('./routes/index'));
app.use('/login', require('./routes/login'));
app.use('/logout', require('./routes/logout'));

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
