const nodeFetch = require('node-fetch');

/**
 * Fetch data using token auth
 * @param {string} url
 * @param {string} token
 * @return {Promise<boolean | JSON>}
 */
async function fetch(url, token) {
    try {
        const response = await nodeFetch(
            url,
            {
                method: 'GET',
                headers: new Headers({
                    Authorization: `bearer ${token}`,
                }),
            },
        );
        return response.json();
    } catch (err) {
        // TODO miky better logging
        console.log('CATCH fetch error', err.message); // eslint-disable-line no-console
        return false;
    }
}

module.exports = fetch;
