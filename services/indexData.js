const fetch = require('./fetch');
const { logout } = require('../auth/auth');

/**
 * Get data from different endpoints and consolidate for view
 *
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @param {NextFunction} next
 * @return {Promise<boolean|{APIs: *, me: (boolean|JSON)}>}
 */
async function getData(req, res, next) {
    const { token } = req.session.tokenData;

    try {
        const me = await fetch('https://api.apiary.io/me', token);

        const { error } = me;
        if (error === 'Token Invalid') {
            req.flash('error', 'Token has expired. Please log in again.');
            logout({ req, res });

            res.redirect('/login');
            return false;
        }
        // TODO miky cover other fetch errors

        const APIs = await fetch('https://api.apiary.io/me/apis', token);
        const data = {
            me,
            APIs: APIs.apis,
        };

        req.data = data;
        next();
        return data;
    } catch (err) {
        // TODO miky better logging
        console.log('CATCH fetch error', err.message); // eslint-disable-line no-console
        return false;
    }
}

module.exports = {
    getData,
};
