# TODO

##code
 * style according "graphic proposal"
 * remove token from Apiary server on logout
 * being able to be persistently logged in more different browsers
    * now every new connection invalidate token for others
 * fetch error handling (try catch todos in code)
 * polling for actual data
 * analytics
 * error reporting

##tests
 * unit
    * token scenarios
        * new token saved to session only
        * new token saved to cookies
        * logout clear session only
        * logout clear cookie & session
 * integration
    * handle request error (no connectivity / 500)
    * handle response without required fields
    * parse correct responses
 * end to end
    * redirect to login page (for unauthenticated user)
    * menu items corresponds with state
    * content rendered for mocked data (no api in list, ...)
    * flash messages rendered
      
##docs
 * app
    * login (remember me behavior)
 * requirements
 * deployment
 * token security
