# Task: UI for Apiary’s public API


Your task is to implement dockerized NodeJS application providing UI for Apiary’s Public API.

API Specification can be found here: https://apiary.docs.apiary.io

The UI should be capable of:
- Sign in user
- Show information about signed in user
- List user’s API Projects

As output you should provide git repository and as bonus deployed application on your favourite provider.

We expect it will be close to production ready application. It is ok to skip parts for time reasons, but you should summarise how would you cover it in real life.