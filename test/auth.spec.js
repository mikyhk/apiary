const { expect } = require('chai');
const auth = require('../auth/auth');

describe('Iron - encapsulated token', () => {
    it('seal and unseal object', async () => {
        const sealedToken = await auth.sealToken({
            token: '123456789',
            tokenDescription: 'desc123',
            isoDate: '2019-08-31T17:37:31.101Z',
        });

        const unsealed = await auth.unsealToken({ sealedToken });

        expect(unsealed).to.deep.equal({
            token: '123456789',
            tokenDescription: 'desc123',
            isoDate: '2019-08-31T17:37:31.101Z',
        });
    });
});
