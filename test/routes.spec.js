const request = require('supertest');
const app = require('../app');

describe('login page rendered', () => {
    it('has the default page', (done) => {
        request(app)
            .get('/login')
            .expect(/Login/, done);
    });
});
